# Mirth Connect

Installing on ubuntu:

    https://www.mirthcorp.com/community/forums/showthread.php?t=218194

# Assynchronous Routes using RabbitMQ + Celery + Flask

CONFIGURE RABBITmq

    sudo apt-get install rabbitmq-server
    sudo rabbitmqctl add_user apiuser apipassword
    sudo rabbitmqctl add_vhost apivhost
    sudo rabbitmqctl set_user_tags apiuser yapitag
    sudo rabbitmqctl set_permissions -p apivhost apiuser ".*" ".*" ".*"		
    
RUN RABBITmq
    
    sudo rabbitmq-server

RUN RABBIT worker
    
    (.venv) celery -A SeverityAPI.severityApi.celery worker --loglevel=info

# Google Cloud

## Kubernetes

Configure kubectl command line tool

    gcloud components install kubectl
    
Deploy an image

    docker build -t gcr.io/${PROJECT_ID}/${REPO_NAME}:$(git log --pretty=format:'%h' -n 1) .
    gcloud auth configure-docker
    docker push gcr.io/${PROJECT_ID}/${REPO_NAME}:$(git log --pretty=format:'%h' -n 1)
    docker run --env-file security.env -p 8000:8000 a078e37e3026


## MLEngine
Batch prediction

    gcloud ml-engine jobs submit prediction $JOB_NAME \
        --model $MODEL_NAME \
        --input-paths $INPUT_PATHS \
        --output-path $OUTPUT_PATH \
        --region $REGION \
        --data-format $DATA_FORMAT \
        --max-worker-count=$MAX_WORKER_COUNT \
        --version=$VERSION


Submit Training Jobs

    gcloud ml-engine jobs submit training $JOB \
        --module-name $MODULE_NAME \
        --config config.yaml \
        --job-dir $JOB_DIR \
        --package-path $PACKAGE_PATH \
        --region us-central1 \
        --runtime-version $RUNTIME_VERSION \
        --scale-tier CUSTOM \
        -- \
        --finding $FINDING \
        --architecture $ARCHITECTURE \
        --dataset $DATASET


Run Local Training

    nohup gcloud ml-engine local train \
        --module-name=$MODULE_NAME \
        --job-dir $JOB_DIR \
        --package-path $PACKAGE_PATH \
        -- \
        --finding $FINDING \
        --architecture $ARCHITECTURE \
        --dataset $DATASET > $JOB_DIR/LOG.txt 2>&1 &
    tail -f $JOB_DIR/LOG.txt


## Serverless SQL DB

Test connection

    With digital certificate:
          psql "sslmode=verify-ca sslrootcert=.certs/server-ca.pem \
              sslcert=.certs/client-cert.pem sslkey=.certs/client-key.pem \
              hostaddr=xxx.xxx.xxx.xxx \
              port=5432 \
              user=postgres dbname=postgres"
    Without digital certificate:
          psql "sslmode=disable dbname=postgres user=postgres hostaddr=xxx.xxx.xxx.xxx"

Postgres Commands

    SELECT table_schema,table_name FROM information_schema.tables; (list tables)
    SELECT * FROM "PatientInfo_patient"; (apply a query)


## Compute Engine

    gcloud compute instances list (list virtual machines)
    gcloud compute instances start (start a VM)
    gcloud compute instances stop (shut down a VM)
    gcloud compute ssh user@instance_name (connect ssh)
    gcloud compute ssh instance_name --project projectname --zone zone_name
    gcloud compute scp instance_name:/path/to/remote/file.png /path/to/local/folder/ (copy remote file to local folder)
    SCP recursive:
        gcloud compute scp --recurse ...
## Bucket

    Copy multiprocessed (-m) ignoring files that already exist (-n from noclobber):
        gsutil -m cp -n large_folder gs://bucket_name

# Git

    git config --global user.name "NDrabowski"
    git config --global user.email nicholas.drabowski@gmail.com
    git clone https://[...].git
    git fetch (download remote branches)
    git branch -r  (list remote branches)
    git checkout -b ... (checkout to remote branch)
    git checkout branch_name (checkout to local branch)
    git checkout -- . (throw all modifications away)
    git checkout -- file.py (throw modifications of only one file away)
    git add file.py (add file to the next commit)
    git reset file.py (opposite of git add)
    git log -1 (check the current commit message and hash)

# Docker

Installation

    sudo apt-get update
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt-get update
    apt-cache policy docker-ce

Usage

    sudo docker build -t image_tag_name . (build docker image using the 'Dockerfile' present in the current directory)
    docker images -a (list all images)
    docker image ls
    docker ps -a (list all containers)
    sudo docker run image_tag_name (create a container from the image and runs it)
    sudo docker run -p 2222:2222 -d image_tag_name (run container maping ports)
    docker stop container_id
    docker rm container_or_image_id
    Delete all dockers and images:
        docker rm $(docker ps -a -q) ; docker rmi -f $(docker images -q)
    sudo docker run --env-file environment_vars.env -p 2222:2222 -v /tmp:/tmp -v /folder_inside_container:/folder_in_the_host


# Docker Compose

Installation

    Check which is the latest docker compose version in: https://github.com/docker/compose/releases
        sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose	
        sudo chmod +x /usr/local/bin/docker-compose
        sudo curl -L https://raw.githubusercontent.com/docker/compose/1.21.2/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose

Usage

    sudo docker-compose up --build (run image)

# Django

    python manage.py startapp PatientInfo (creates new app)
    python manage.py makemigrations (generates the migration)
    python manage.py migrate (applies to the database)
    python manage.py runserver
    python manage.py createsuperuser

# Python

Sphynx

    apt-get install python-sphinx
    make html
    sphinx-apidoc -o <outputdir> <inputpackage>

Virtual Environments

    PYTHON 2.7:
        virtualenv env (create a virtual environment)
        source env/bin/activate (activate it)
        deactivate (deactivate it)
    PYTHON 3.6:
        sudo add-apt-repository ppa:jonathonf/python-3.6
        sudo apt-get update
        sudo apt-get install python3.6
        virtualenv --python=/usr/bin/python3.6 env36
        python3 -m venv env
        source env36/bin/activate
        pip3 install -r requirements.txt

    Load all environment variables from file
        export $(grep -v '^#' .env | xargs)

# Misc

Run script detached from terminal with log file

    nohup python -u script.py >> file_to_save_log.txt 2>&1 &
    tail -f file_to_save_log.txt

Test Cors with curl

    curl -H "Origin: http://35.188.116.227/" \
     -H "Access-Control-Request-Method: POST" \
     -H "Access-Control-Request-Headers: X-Requested-With" \
     -X OPTIONS --verbose http://35.188.116.227:8080/logs/ailaRequests/lastRequests=3

## REGEX

    Keywords:
        \d (any number)
        \D (everything except numbers)
        \w (any letter)
        \W (everything except letters)
        . (any characters)
    Set of characters:
        /[abcdfg]/
    Quantifiers:
        /a?/ (optional 'a')
        /a+/ (one or more 'a')
        /a{2,4}/ (from 2 to 4 times)
    Negate characters:
        /[^abcd]/
    Special characters:
        /[[:space:]]/
    Grouping:
        /^(Ann|Frank)$/

## SED

    Usage:
        sed 's/FindthisWord/ReplaceWithThisWord/g' input.txt > output.txt
    Replace double quotes by single:
        sed "s/\"/'/g"
    Replace only first ocurrence of pattern:
        sed 's/old_pattern/new_pattern/'
    Compare the stuff case insensitively:
        sed 's/old_pattern/new_pattern/gi'
    Do the substitution only in specific lines:
        sed '2,4 s/old/new/g'
    Do the stuff untill the end of file:
        sed '2,$ s/old/new/g'
    Specify several expressions:
        sed -r -e 's/c1/d1/g' -e 's/a2/b2/g'
    Refer to parethesis expression for the substitution:
        Input: abc12dbjh3xyz
        sed -r 's/(abc).+(xyz)/\2_\1/g'
        Output: xyz_abc

## Dealing with csv files

    Get few lines:
        head -n 1000 table_in.csv > table_out.csv
    See it as table:
        column -s, -t < table.csv | less -#2 -N -S
    Remove specific lines:
        sed '2,4d'  (remove lines 2 through 4)
    See only specific lines:
        sed -n '2,4p' (negate option + print specific lines)
    Delete lines by pattern:
        sed -r '/pattern/d'
    Filter lines by pattern:
        sed -r -n '/pattern/p'

## Other

Copy to clipboard

    echo test | xclip -selection c

Environment variables from file

    export $(grep -v '^#' .env | xargs)